import java.util.Scanner;

public class AdministratorUI {
    private Administrator admin;

    public AdministratorUI(Administrator admin) {
        this.admin = admin;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Administrator Authentication:");
        System.out.print("Username: ");
        String username = scanner.nextLine();
        System.out.print("Password: ");
        String password = scanner.nextLine();

        admin.authenticate(username, password);

        if (admin.isAuthenticated()) {
            displayMenu(scanner);
        } else {
            System.out.println("Authentication failed. Exiting...");
        }
    }

    private void displayMenu(Scanner scanner) {
        while (true) {
            System.out.println("\nAdministrator Menu:");
            System.out.println("1. Add Element");
            System.out.println("2. Delete Element");
            System.out.println("3. Add Category");
            System.out.println("4. Display Database State");
            System.out.println("5. Exit");

            System.out.print("Select an option: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (choice) {
                case 1:
                    System.out.print("Enter element name: ");
                    String elementName = scanner.nextLine();
                    admin.addRecord(new Element(elementName));
                    break;
                case 2:
                    // Similar logic for deleting elements
                    break;
                case 3:
                    System.out.print("Enter category name: ");
                    String categoryName = scanner.nextLine();
                    admin.addCategory(new Category(categoryName));
                    break;
                case 4:
                    admin.getDatabase().displayDatabaseState();
                    break;
                case 5:
                    System.out.println("Exiting Administrator Menu...");
                    return;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }
}
