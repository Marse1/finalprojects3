public class Main {
    public static void main(String[] args) {
        Database database = new Database();
        Administrator admin = new Administrator(database);

        // Simulate user input for authentication
        admin.authenticate("admin", "password");
        database.addObserver(admin);
        // Simulate adding a new category and element
        admin.addCategory(new Category("Electronics"));
        admin.addRecord(new Element("Laptop"));

        // Display current database state
        System.out.println("Current Database State:");
        database.displayDatabaseState();

        AdministratorUI adminUI = new AdministratorUI(admin);
        adminUI.start();
    }
}
