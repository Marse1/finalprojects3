public interface DatabaseObserver {
    void update();

    Database getDatabase();
}
