import java.util.Scanner;

public class Administrator implements DatabaseObserver  {
    private boolean authenticated;
    private Database database;

    public Administrator(Database database) {
        this.authenticated = false;
        this.database = database;
    }

    public void authenticate(String username, String password) {
        // For simplicity, let's use hardcoded values for demo purposes
        if ("admin".equals(username) && "password".equals(password)) {
            authenticated = true;
            System.out.println("Authentication successful!");
        } else {
            System.out.println("Authentication failed. Please try again.");
        }
    }

    public void addRecord(Element element) {
        if (authenticated) {
            database.addElement(element);
            System.out.println("Record added successfully.");
        } else {
            System.out.println("Authentication required to perform this action.");
        }
    }

    public void deleteRecord(Element element) {
        if (authenticated) {
            database.deleteElement(element);
            System.out.println("Record deleted successfully.");
        } else {
            System.out.println("Authentication required to perform this action.");
        }
    }

    public void addCategory(Category category) {
        if (authenticated) {
            database.addCategory(category);
            System.out.println("Category added successfully.");
        } else {
            System.out.println("Authentication required to perform this action.");
        }
    }
    @Override
    public void update() {
        // Handle real-time updates here
        System.out.println("Database updated. Refresh your view.");
    }

    @Override
    public Database getDatabase() {
        return null;
    }

    public boolean isAuthenticated() {
        return false;
    }
}
