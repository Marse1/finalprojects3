import java.awt.*;
import java.util.ArrayList;

public class Database {
    private List<Element> elements;
    private List<Category> categories;

    public Database() {
        this.elements = new ArrayList<>();
        this.categories = new ArrayList<>();
    }
    public List<Category> getCategories() {
        return categories;
    }
    private List<DatabaseObserver> observers = new ArrayList<>();
    public void addElement(Element element) {
        elements.add(element);
        notifyObservers();
    }

    public void deleteElement(Element element) {
        elements.remove(element);
        notifyObservers();
    }

    public void addCategory(Category category) {
        categories.add(category);
        notifyObservers();
    }

    public List<Element> getElements() {
        return elements;
    }

    public void addObserver(DatabaseObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(DatabaseObserver observer) {
        observers.remove(observer);
    }

    private void notifyObservers() {
        for (DatabaseObserver observer : observers) {
            observer.update();
        }
    }
    // Additional methods for database interaction

    public void displayDatabaseState() {
        System.out.println("Categories:");
        for (Category category : categories) {
            System.out.println(category.getName());
        }

        System.out.println("\nElements:");
        for (Element element : elements) {
            System.out.println(element.getName());
        }
    }
}